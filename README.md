# Bedappy remix of Behat API Extension

This is an attempt to add Bedappy functionality onto Behat API Extension with a view to future PR, if desired upstream. The majority of behat-api-extension code will be
stripped back out, and added as a dependency (as per extending-the-extension).

**Note that this cross-over repository  of Bedappy is currently primarily the upstream Imbo\BehatApiExtension package - you almost certainly are looking for that package!**

# Upstream README

[![Current build Status](https://secure.travis-ci.org/imbo/behat-api-extension.png)](http://travis-ci.org/imbo/behat-api-extension)
[![Latest Stable Version](https://poser.pugx.org/imbo/behat-api-extension/version)](https://packagist.org/packages/imbo/behat-api-extension)
[![License](https://poser.pugx.org/imbo/behat-api-extension/license)](https://packagist.org/packages/imbo/behat-api-extension)

This Behat extension provides an easy way to test JSON-based API's in [Behat 3](http://behat.org). Inspired by [behat/web-api-extension](https://github.com/Behat/WebApiExtension/) and originally written to test the [Imbo API](http://imbo.io).

## Installation / Configuration / Documentation

End-user docs can be found [here](https://behat-api-extension.readthedocs.io/).

## Copyright / License

Copyright (c) 2016-2017, Christer Edvartsen <cogo@starzinger.net>

Licensed under the MIT License
