<?php

namespace Flaxandteal\Bedappy\Context;

use App;
use Artisan;
use Auth;
use Carbon\Carbon;
use DB;
use ReflectionException;
use stdClass;
use Cache;
use Hash;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Laracasts\Behat\Context\DatabaseTransactions;
use PHPUnit_Framework_Assert as PHPUnit;
use Imbo\BehatApiExtension\ArrayContainsComparator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Behat\Behat\Context\SnippetAcceptingContext;
use Imbo\BehatApiExtension\Context\ArrayContainsComparatorAwareContext;
use Exception;
use Behat\Mink\Mink;
use PHPUnit\Framework\Assert;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Client;

/**
 * Defines application features from the specific context.
 */
class ApiContext extends RawMinkContext implements RestfulContext, ArrayContainsComparatorAwareContext, SnippetAcceptingContext
{

    /**
     * Instance of the comparator that handles matching of JSON
     *
     * @var ArrayContainsComparator
     */
    protected $arrayContainsComparator;

    /**
     * {@inheritdoc}
     */
    public function setArrayContainsComparator(ArrayContainsComparator $comparator) {
        $this->arrayContainsComparator = $comparator;

        return $this;
    }

    /**
     * Set the User model
     *
     * @param string $userModel
     * @return self
     */
    function setUserModel($userModel)
    {
        $this->userModel = $userModel;

        return $this;
    }

    /**
     * Set the prefix for API routes
     *
     * Unlike the Behat API extension, we do not want an FQDN.
     *
     * @param string $apiPrefix
     * @return self
     */
    function setApiPrefix($apiPrefix)
    {
        $this->apiPrefix = $apiPrefix;

        return $this;
    }

    /**
     * Set the prefix for RESTful models
     *
     * @param string $modelPrefix
     * @return self
     */
    function setModelPrefix($modelPrefix)
    {
        $this->modelPrefix = $modelPrefix;

        return $this;
    }

    /**
     * While a state machine like this wouldn't be ideal in the code itself,
     * here it provides a useful flow for the natural-language-like
     * syntax of Gherkin
     */
    protected $_activeObject = null;

    protected $_lastAlready = [];

    protected $_response = null;

    protected $_pointInTime = null;

    protected $_token = null;

    protected $_known = [];

    protected $_knownId = [];

    protected $_knownIds = [];

    protected $_parameters = [];

    /**
     * @When /^I set parameter "([^"]*)" to the ID of this (.*)$/
     */
    public function iSetParameterToTheID($arg1, $arg2)
    {
        $this->_parameters[$arg1] = $this->_knownId[$arg2];
    }

    public function request($method, $uri, $parameters, $form=false, $content=null)
    {
        $parameters = array_merge($this->_parameters, $parameters);

        $driver = $this->getSession()
            ->getDriver();
        $client = $driver
            ->getClient();
        $client->followRedirects(false);

        $contentType = 'application/json';

        if ($form) {
            $contentType = 'application/x-www-form-urlencoded';
        }

        $headers = [
            'CONTENT_TYPE' => $contentType,
            'HTTP_X_CSRF_TOKEN' => $this->_token,
            'HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest'
        ];
        $request = $client->request(
            $method,
            $uri,
            $parameters,
            [],
            $headers,
            $content
        );

        $type = 'text/html';
        $responseHeaders = $driver->getResponseHeaders();
        if (array_key_exists('content-type', $responseHeaders)) {
            $type = $responseHeaders['content-type'][0];
            // Strip charset if required
            if (strpos($type, ';')) {
                $type = substr($type, 0, strpos($type, ';'));
            }
        }

        $contentText = $driver->getContent();
        $data = null;
        if ($type == 'application/json') {
            $content = json_decode($contentText, true);
            if (isset($content['data'])) {
                $data = $content['data'];

                if (count($data) > 1 || array_keys($data) != ['0']) {
                    $data = json_decode($contentText)->data;
                }
            }
        } else {
            $content = $contentText;
        }

        return [
            'status' => $driver->getStatusCode(),
            'type' => $type,
            'content' => $content,
            'data' => $data
        ];
    }

    /**
     * @Given /^I want to ([\w-]+) (a|the|this) ([\w]*) of this (.*) through the API$/
     */
    public function iWantToTheOfThisThroughTheAPI($arg1, $prep, $arg2, $arg3)
    {
        $type = camel_case($arg3);
        $this->_url = str_plural($type) . '/' . $this->_knownId[$arg3];
        $this->_activeObject = null;

        switch ($arg1) {
        case 'update':
            $this->_method = 'put';
            $this->_isForm = true;
            $this->_sendObject = false;
            $this->_url .= '/' . str_plural(camel_case($arg2)) . '/' . $this->_knownId[$arg2];
            break;
        case 'destroy':
            $this->_method = 'delete';
            $this->_isForm = false;
            $this->_sendObject = false;
            $this->_url .= '/' . str_plural(camel_case($arg2)) . '/' . $this->_knownId[$arg2];
            break;
        case 'show':
            $this->_method = 'get';
            $this->_isForm = false;
            $this->_sendObject = false;
            $this->_url .= '/' . str_plural(camel_case($arg2)) . '/' . $this->_knownId[$arg2];
            break;
        case 'bulk-update':
            $this->_method = 'put';
            $this->_isForm = true;
            $this->_sendObject = true;
            $this->_url .= '/' . str_plural(camel_case($arg2));
            break;
        case 'store':
            $this->_method = 'post';
            $this->_isForm = true;
            $this->_sendObject = false;
            $this->_url .= '/' . str_plural(camel_case($arg2));
            break;
        case 'list':
            $this->_method = 'get';
            $this->_isForm = false;
            $this->_sendObject = false;
            $this->_url .= '/' . str_plural(camel_case($arg2));
            break;
        default:
            throw new Exception("Unknown API verb for specific submodel request");
        }
    }

    /**
     * @Given /^I want to ([\w]+) this (.*) through the API$/
     */
    public function iWantToThisThroughTheAPI($arg1, $arg2)
    {
        $type = camel_case($arg2);
        $this->_url = str_plural($type) . '/' . $this->_knownId[$arg2];
        $this->_activeObject = null;

        switch ($arg1) {
        case 'update':
            $this->_method = 'put';
            $this->_isForm = true;
            $this->_sendObject = false;
            break;
        case 'destroy':
            $this->_method = 'delete';
            $this->_isForm = false;
            $this->_sendObject = false;
            break;
        case 'show':
            $this->_method = 'get';
            $this->_isForm = false;
            $this->_sendObject = false;
            break;
        default:
            throw new Exception("Unknown API verb for specific ('this') request");
        }
    }

    /**
     * @Given /^I want to ([\w-]+) ?a?n? ([A-Z].*) through the API$/
     */
    public function iWantToAThroughTheAPI($arg1, $arg2)
    {
        $this->_url = camel_case(str_plural($arg2));
        $this->_activeObject = null;

        switch ($arg1) {
            case 'bulk-update':
                $this->_method = 'put';
                $this->_isForm = true;
                $this->_sendObject = true;
                break;
            case 'store':
                $this->_method = 'post';
                $this->_isForm = true;
                $this->_sendObject = false;
                break;
            case 'show':
                $this->_method = 'get';
                $this->_isForm = false;
                $this->_sendObject = false;
                break;
            case 'list':
                $this->_method = 'get';
                $this->_isForm = false;
                $this->_sendObject = false;
                break;
            default:
                throw new Exception("Unknown API verb for unspecific ('a') request");
        }
    }

    /**
     * @Given the response should be an array
     */
    public function theResponseShouldBeAnArray()
    {
        Assert::assertTrue(is_array($this->_response['data']));
    }

    /**
     * @Given /^the response should be an array of length (\d*)$/
     */
    public function theResponseShouldBeAnArrayOfLength($arg1)
    {
        $this->theResponseShouldBeAnArray();

        Assert::assertEquals($arg1, count($this->_response['data']));
    }

    /**
     * @When I send a request with query:
     */
    public function iSendARequestWithQuery(PyStringNode $string)
    {
        $string = $this->replaceKnownIds($string);

        $query = json_decode((string)$string);

        $this->iSendARequest($query);
    }

    /**
     * @When I send a request
     */
    public function iSendARequest($query = null)
    {
        $targetUrl = url($this->apiPrefix . '/' . $this->_url);

        if ($query) {
            // Very basic for current purposes
            // TODO: tidy up for general usage
            $targetUrl .= '?';
            $targetParameters = [];
            foreach ($query as $key => $value) {
                $targetParameters[] = e($key) . '=' . e($value);
            }
            $targetUrl .= implode('&', $targetParameters);
        }

        $fields = [];
        $content = null;
        if ($this->_activeObject) {
            if ($this->_isForm) {
                $fields = (array)$this->_activeObject;
            }
            if ($this->_sendObject) {
                $content = json_encode($this->_activeObject);
            }
        }
        print_r("[sending " . $this->_method . " to {$targetUrl}]");

        $this->_response = $this->request($this->_method, $targetUrl, $fields, $this->_isForm, $content);
    }

    /**
     * @Given /^this (.*)'s ([^,]*) already has known ID and these properties:$/
     */
    public function thisWhichHasKnownIdAlreadyHasProperties($arg1, $arg2, PyStringNode $string)
    {
        $obj = $this->thisAlreadyHasProperties($arg1, $arg2, $string);

        $this->addKnownId($arg2, $obj);
    }

    /**
     * @Given /^this (.*)'s (.*) already has properties:$/
     */
    public function thisAlreadyHasProperties($arg1, $arg2, PyStringNode $string)
    {
        $parent = $this->_lastAlready[$arg1];

        $attribute = camel_case($arg2);

        $string = $this->replaceKnownIds($string);

        $fields = json_decode($string, true);
        if (!$fields) {
            throw new Exception("Could not convert from JSON: " . $string);
        }
        $obj = $parent->{$attribute};
        $obj->forceFill($fields);

        $parent->{$attribute}->save();

        $this->_lastAlready[$arg2] = $obj;

        return $obj;
    }

    /**
     * @Then /^I note that "([^"]*)" from the last response is the ID of an? (\w*)$/
     */
    public function iNoteThatFromTheLastResponseIsTheIdOfA($arg1, $arg2)
    {
        try {
            $obj = App::make($this->modelPrefix . ucfirst(camel_case($arg2)));
        } catch (ReflectionException $e) {
            $obj = (object)['id' => null];
        }
        $content = $this->_response['data'];

        $obj->id = $content->{$arg1};

        $this->addKnownId($arg2, $obj);

        $this->_lastAlready[$arg2] = $obj;
    }

    /**
     * @Given /^I already have an? ([^,]*), with known ID:$/
     */
    public function iAlreadyHaveAWithKnownID($arg1, PyStringNode $string)
    {
        $obj = $this->iAlreadyHaveA($arg1, $string);

        $this->addKnownId($arg1, $obj);

        $this->_lastAlready[$arg1] = $obj;
    }

    protected function addKnownId($name, $obj)
    {
        $this->_knownId[$name] = $obj->id;
        $this->_known[$name] = $obj;
        if (!array_key_exists($name, $this->_knownIds)) {
            $this->_knownIds[$name] = [];
        }
        $this->_knownIds[$name][] = $obj->id;
    }

    /**
     * Swap KNOWN_ID tokens for known IDs
     *
     * @param string $string
     */
    protected function replaceKnownIds($string)
    {
        $string = preg_replace('/^(DB|JSON)/', '', $string);
        foreach ($this->_knownId as $type => $id) {
            $string = str_replace('{KNOWN_ID:' . $type . '}', '"' . $id . '"', $string);
        }
        foreach ($this->_knownIds as $type => $arr) {
            foreach ($arr as $ix => $id) {
                $string = str_replace('{KNOWN_ID:' . $type . ':' . ($ix + 1) . '}', '"' . $id . '"', $string);
            }
        }

        return $string;
    }

    /**
     * @Given /^I already have an? ([^,]*):$/
     */
    public function iAlreadyHaveA($arg1, PyStringNode $string)
    {
        $obj = App::make($this->modelPrefix .ucfirst(camel_case($arg1)));

        $string = $this->replaceKnownIds($string);

        $fields = json_decode($string, true);
        if (!$fields) {
            throw new Exception("Could not convert from JSON: " . $string);
        }
        $obj->forceFill($fields);
        $obj->save();

        $this->_lastAlready[$arg1] = $obj;

        return $obj;
    }

    protected static function arrayDiff($arr1, $arr2)
    {
        $missingArrays = [];
        $shouldContainWithoutNested = [];
        foreach ($arr1 as $key => $value) {
            if (is_array($value)) {
                if (!array_key_exists($key, $arr2)) {
                    $missingArrays[$key] = [$value, null];
                } else if (!is_array($arr2[$key])) {
                    $missingArrays[$key] = [$value, $arr2[$key]];
                } else {
                    $missingNestedArrays = self::arrayDiff($value, $arr2[$key]);
                    if ($missingNestedArrays) {
                        $missingArrays[$key] = $missingNestedArrays;
                    }
                }
            } else {
                $shouldContainWithoutNested[$key] = $value;
            }
        }
        $missingItems = array_diff_assoc($shouldContainWithoutNested, $arr2);

        return array_merge($missingItems, $missingArrays);
    }

    /**
     * @Then /^the response should(.*) contain JSON:$/
     */
    public function theResponseShouldContainJSON($arg1, PyStringNode $string)
    {
        $this->theResponseShouldBeJSON();

        $string = $this->replaceKnownIds($string);

        $shouldContain = json_decode((string)$string, true);

        if ($shouldContain === null) {
            throw new Exception("JSON string is not valid:\n$string");
        }

        // We use this contrast to allow the natural JSON interpretation
        // _and_ the more easily introspectable array version for checking
        // against
        $content = $this->_response['data'];
        $contentAsArray = $this->_response['content']['data'];

        if (is_array($content)) {
            $found = false;
            foreach ($contentAsArray as $entry) {
                $found |= empty($this->arrayDiff($shouldContain, $entry));
            }
            $missingItems = $found ? [] : $shouldContain;
        } else {
            $missingItems = $this->arrayDiff($shouldContain, $contentAsArray);
        }

        if (trim($arg1) == 'not') {
            Assert::assertNotEmpty($missingItems, "Response matching: " . json_encode($content));
        } else {
            Assert::assertEmpty($missingItems, "Response is not matching: " . json_encode($missingItems, JSON_PRETTY_PRINT) . "\n" . json_encode($content, JSON_PRETTY_PRINT));
        }
    }

    /**
     * @Then /^the response should not have an? "([^"]*)" property$/
     */
    public function theResponseShouldNotHaveAProperty($arg1)
    {
        if (is_array($this->_response['data'])) {
            foreach ($this->_response['data'] as $entry) {
                $entry = json_decode(json_encode($entry));
                Assert::assertObjectNotHasAttribute($arg1, $entry);
            }
        } else {
            Assert::assertObjectNotHasAttribute($arg1, $this->_response['data']);
        }
    }

    /**
     * @Then /^the response should have an? "([^"]*)" property$/
     */
    public function theResponseShouldHaveAProperty($arg1)
    {
        if (is_array($this->_response['data'])) {
            foreach ($this->_response['data'] as $entry) {
                Assert::assertObjectHasAttribute($arg1, $entry);
            }
        } else {
            Assert::assertObjectHasAttribute($arg1, $this->_response['data']);
        }
    }

    /**
     * @Then /^the response should have an? "([^"]*)" property, which is the ID of this (.*)$/
     */
    public function theResponseShouldHaveAPropertyWhichIsThisIdOfThis($arg1, $arg2)
    {
        $this->theResponseShouldHaveAProperty($arg1);

        Assert::assertEquals($this->_response['data']->{$arg1}, $this->_knownId[$arg2]);
    }

    /**
     * @Then /^the response should have an? "([^"]*)" property, which is an? (.*)$/
     */
    public function theResponseShouldHaveAPropertyWhichIsA($arg1, $arg2)
    {
        $this->theResponseShouldHaveAProperty($arg1);

        $value = $this->_response['data']->{$arg1};

        switch ($arg2) {
        case 'number':
            Assert::assertTrue(is_numeric($value));
            break;
        case 'uuid':
            // TODO: tighten this up for general applicability
            Assert::assertRegExp('/^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/', $value);
            break;
        default:
            throw new Exception("Do not know how to test for {$arg2}");
        }
    }

    /**
     * @Then the response should be forbidden
     */
    public function theResponseShouldBeForbidden()
    {
        if ($this->_response['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->_response['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            403,
            $this->_response['status'],
            $message
        );
    }

    /**
     * @Then the response should be redirected
     */
    public function theResponseShouldBeRedirected()
    {
        if ($this->_response['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->_response['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            302,
            $this->_response['status'],
            $message
        );
    }

    /**
     * @Then the response should be unprocessable because :arg1 has problem :arg2
     */
    public function theResponseShouldBeUnprocessableBecause($arg1, $arg2)
    {
        $this->theResponseShouldBeUnprocessable();

        Assert::assertEquals(implode(', ', $this->_response['content'][$arg1]), $arg2);
    }

    /**
     * @Then the response should be unprocessable
     */
    public function theResponseShouldBeUnprocessable()
    {
        Assert::assertEquals(
            422,
            $this->_response['status'],
            "Response was not as expected"
        );
    }

    /**
     * @Then the response should be unauthorized
     */
    public function theResponseShouldBeUnauthorized()
    {
        if ($this->_response['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->_response['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            401,
            $this->_response['status'],
            $message
        );
    }

    /**
     * @Then the response should be successful creation
     */
    public function theResponseShouldBeSuccessfulCreation()
    {
        if ($this->_response['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->_response['content']);
        } else {
            $message = "Request did not indicate successful creation";
        }

        Assert::assertEquals(
            201,
            $this->_response['status'],
            $message
        );
    }

    /**
     * @Then the response should be successful
     */
    public function theResponseShouldBeSuccessful()
    {
        if ($this->_response['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->_response['content']);
        } else {
            $message = "Request was not successful";
        }

        Assert::assertEquals(
            200,
            $this->_response['status'],
            $message
        );
    }

    /**
     * @Then the response should be missing
     */
    public function theResponseShouldBeMissing()
    {
        if ($this->_response['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->_response['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            404,
            $this->_response['status'],
            $message
        );
    }

    /**
     * @Then the response should be JSON
     */
    public function theResponseShouldBeJSON()
    {
        Assert::assertEquals(
            'application/json',
            $this->_response['type']
        );
    }

    /**
     * @Given I have an API token
     */
    public function iHaveAnApiToken()
    {
        $client = $this->getSession()
            ->getDriver()
            ->getClient();
        $request = $client->request('GET', '/');
        $this->_token = csrf_token();
    }

    /**
     * @Given its properties will be:
     */
    public function itsPropertiesWillBe(PyStringNode $string)
    {
        $string = $this->replaceKnownIds($string);

        $this->_activeObject = json_decode((string)$string, true);

        if ($this->_activeObject === null) {
            abort(
                1,
                "The property object did not contain valid JSON " .
                "(check any known IDs are, for example, capitalized for " .
                "models and small for roles):\n" . (string)$string
            );
        }
    }

    /**
     * @Given /^it will have a "([^"]*)" property for this (.*)$/
     */
    public function itWillHaveAPropertyForThis($arg1, $arg2)
    {
        if (!$this->_activeObject) {
            $this->_activeObject = new \stdClass;
        }
        $this->_activeObject->{$arg1} = $this->_knownId[$arg2];
    }

    /**
     * @Given /^this (.*) already has this (.*) as a (.*)$/
     */
    public function thisAlreadyHasThisAsA($arg1, $arg2, $arg3)
    {
        $obj = $this->_known[$arg1];
        $related = $this->_known[$arg2];
        $obj->{$arg3}()->attach($related);
    }

    /**
     * @Given /^I have this (.*) as one of my (.*)$/
     */
    public function iHaveThisAsOneOfMy($arg1, $arg2)
    {
        $user = Auth::user();
        $related = $this->_known[$arg1];
        $user->{$arg2}()->attach($related);
    }

    /**
     * @Given /^my "([^"]*)" becomes (.*)$/
     */
    public function myBecomes($arg1, $arg2)
    {
        $user = Auth::user();
        if ($arg2 == "this point in time") {
            $this->_pointInTime = Carbon::now();
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } elseif ($arg2 == "ten seconds ago") {
            $this->_pointInTime = Carbon::now()->subSeconds(10);
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } else {
            $value = json_decode($arg2);
        }
        $user->{$arg1} = $value;
        $user->save();
    }

    /**
     * @Then /^my "([^"]*)" is (.*)$/
     */
    public function myIs($arg1, $arg2)
    {
        if ($arg2 == "that point in time") {
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } else {
            $value = json_decode($arg2);
        }
        Assert::assertEquals(Auth::user()->{$arg1}, $value, Auth::user()->{$arg1});
    }

    /**
     * @Then /^my "([^"]*)" isn't (.*)$/
     */
    public function myIsnt($arg1, $arg2)
    {
        if ($arg2 == "that point in time") {
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } else {
            $value = json_decode($arg2);
        }
        Assert::assertNotEquals(Auth::user()->{$arg1}, $value, Auth::user()->{$arg1});
    }

    /**
     * Begin a database transaction.
     *
     * @BeforeScenario
     */
    public static function beginTransaction()
    {
        $migrator = app('migrator');
        if (! $migrator->repositoryExists()) {
            app('migration.repository')->createRepository();
        }
        $paths = array_merge($migrator->paths(), [app()->databasePath().DIRECTORY_SEPARATOR.'migrations']);
        $migrator->run($paths);

        DB::beginTransaction();

        app('DatabaseSeeder')->run();

        $permissionRegistrar = App::make(\Spatie\Permission\PermissionRegistrar::class);
        $permissionRegistrar->registerPermissions();
    }

    /**
     *
     * Roll it back after the scenario.
     *
     * @AfterScenario
     */
    public static function rollback()
    {
        DB::rollback();
        Cache::flush();
    }

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Then I can do something with Laravel
     */
    public function iCanDoSomethingWithLaravel()
    {
        PHPUnit::assertEquals('.env.behat', app()->environmentFile());
        PHPUnit::assertEquals('acceptance', env('APP_ENV'));
        PHPUnit::assertTrue(config('app.debug'));
    }

    /**
     * @Given the user :arg1
     */
    public function theUser($arg1)
    {
        $user = App::make($this->userModel);
        $user->create([
            'email' => $arg1,
            'name' => "Known Name",
            'password' => Hash::make('password')
        ]);
    }

    /**
     * @Given I am logged out
     */
    public function iAmLoggedOut()
    {
        Auth::logout();
    }

    /**
     * @Given there is a user :arg1
     */
    public function thereIsAUser($arg1)
    {
        $user = App::make($this->userModel);
        $existing = $user->whereEmail($arg1)->first();

        if ($existing) {
            $user = $existing;
        } else {
            $user->fill([
                'email' => $arg1,
                'name' => "Login User",
                'password' => Hash::make('password')
            ]);
            $user->save();
        }

        $this->addKnownId('User', $user);
        return $user;
    }

    /**
     * @Given /^there is a user "([^"]*)", who is an? (.*)$/
     */
    public function thereIsAUserWhoIsA($arg1, $arg2)
    {
        $user = $this->thereIsAUser($arg1);
        $roles = explode(' ', $arg2);
        foreach ($roles as $role) {
            if (! $user->hasRole($role)) {
                $user = $user->assignRole($role);
            }
        }
        $user->load('roles');
        foreach ($roles as $role) {
            $this->addKnownId($role, $user);
        }
        return $user;
    }

    /**
     * @Given I am logged in as :arg1
     */
    public function iAmLoggedInAs($arg1)
    {
        $user = $this->thereIsAUser($arg1);
        Auth::login($user);
        return $user;
    }

    /**
     * @Given /^I am logged in as "([^"]*)", who is an? (.*)$/
     */
    public function iAmLoggedInAsWhoIsA($arg1, $arg2)
    {
        $user = $this->thereIsAUserWhoIsA($arg1, $arg2);
        Auth::login($user);
        return $user;
    }

    /**
     * @When I post a reset form to :arg1 with content:
     */
    public function iPostAResetFormToWithContent($arg1, PyStringNode $string)
    {
        $parameters = json_decode($string, true);

        $driver = $this->getSession()
            ->getDriver();
        $client = $driver
            ->getClient();
        $client->followRedirects(false);
        $driver->visit('/');

        $parameters['token'] = $this->_token;
        $parameters['_token'] = csrf_token();

        $contentType = 'application/x-www-form-urlencoded';
        $headers = [
            'CONTENT_TYPE' => $contentType,
        ];
        $request = $client->request(
            'post',
            $arg1,
            $parameters,
            [],
            $headers
        );
    }

    /**
     * @When :arg1 requests a reset link
     */
    public function requestsAResetLink($arg1)
    {
        $user = App::make($this->userModel);
        $user->whereEmail($arg1)->firstOrFail();
        $broker = App::make('auth.password');
        $this->_token = $broker->createToken($user);
    }

    /**
     * @When :arg1 follows a reset link
     */
    public function followsAResetLink($arg1)
    {
        $this->requestsAResetLink($arg1);
        $this->visitPath('password/reset/' . $this->_token);
    }

    /**
     * @Then the response should be redirected to :arg1
     */
    public function theResponseShouldBeRedirectedTo($arg1)
    {
        $driver = $this->getSession()
            ->getDriver();

        Assert::assertEquals(
            302,
            $driver->getStatusCode()
        );

        Assert::assertEquals(
            $driver->getResponseHeaders()['location'][0],
            $arg1
        );
    }

    /**
     * @Given server-side HTTP requests get dummy responses:
     */
    function serverSideHttpRequestsGetDummyResponses(PyStringNode $string)
    {
        $string = $this->replaceKnownIds($string);

        $responseData = json_decode((string)$string);
        if (! is_array($responseData)) {
            $responseData = [$responseData];
        }

        $queue = array_map(
            function ($response) {
                return function ($req, $opt) use ($response) {
                    return new Response(200, [], json_encode($response), "1.1", null);
                };
            },
            $responseData
        );
        $handler = ["handler" => new MockHandler($queue)];
        $client = new Client($handler);
        app()->instance(Client::class, $client);
    }
}
