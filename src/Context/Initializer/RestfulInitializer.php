<?php
namespace Flaxandteal\Bedappy\Context\Initializer;

use Flaxandteal\Bedappy\Context\RestfulContext;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\Initializer\ContextInitializer;

/**
 * RESTful initializer
 *
 * Initializer for feature contexts that implement the RestfulAware interface.
 *
 * @author Phil Weir <phil.weir@flaxandteal.co.uk>
 */
class RestfulInitializer implements ContextInitializer {
    /**
     * @var string
     */
    private $modelPrefix;

    /**
     * Class constructor
     *
     * @param string $baseUri
     */
    public function __construct($apiPrefix, $modelPrefix, $userModel) {
        $this->apiPrefix = $apiPrefix;
        $this->modelPrefix = $modelPrefix;
        $this->userModel = $userModel;
    }

    /**
     * Initialize the context
     *
     * Inject the Guzzle client if the context implements the ApiClientAwareContext interface
     *
     * @param Context $context
     */
    public function initializeContext(Context $context) {
        if ($context instanceof RestfulContext) {
            $context->setApiPrefix($this->apiPrefix);
            $context->setModelPrefix($this->modelPrefix);
            $context->setUserModel($this->userModel);
        }
    }
}
