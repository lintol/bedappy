<?php
namespace Flaxandteal\Bedappy\Context;

use Behat\Behat\Context\Context;
use GuzzleHttp\ClientInterface;

/**
 * Api client aware interface
 *
 * @author Christer Edvartsen <cogo@starzinger.net>
 */
interface RestfulContext extends Context {
    /**
     * Set the prefix for API routes
     *
     * Unlike the Behat API extension, we do not want an FQDN.
     *
     * @param string $apiPrefix
     * @return self
     */
    function setApiPrefix($apiPrefix);

    /**
     * Set the prefix for RESTful models
     *
     * @param string $modelPrefix
     * @return self
     */
    function setModelPrefix($modelPrefix);

    /**
     * Set the user model for RESTful models
     *
     * @param string $userModel
     * @return self
     */
    function setUserModel($userModel);
}
