<?php
namespace Flaxandteal\Bedappy\ServiceContainer;

use Behat\Behat\Context\ServiceContainer\ContextExtension;
use Behat\Testwork\ServiceContainer\Extension as ExtensionInterface;
use Behat\Testwork\ServiceContainer\ExtensionManager;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use GuzzleHttp\ClientInterface;
use InvalidArgumentException;

/**
 * Behat API extension
 *
 * This extension provides a series of steps that can be used to easily test API's. The ApiContext
 * class also exposes the client, request and response objects so custom steps using the underlying
 * client can be implemented.
 *
 * @author Phil Weir <phil.weir@flaxandteal.co.uk>
 * )heavily based on work of Christer Edvartsen <cogo@starzinger.net>)
 */
class BedappyExtension implements ExtensionInterface {
    /**
     * Service ID for the RESTful initializer
     *
     * @var string
     */
    const RESTFUL_INITIALIZER_SERVICE_ID = 'bedappy.restful.context_initializer';

    /**
     * Config key for the extension
     *
     * @var string
     */
    const CONFIG_KEY = 'bedappy';

    /**
     * {@inheritdoc}
     */
    public function getConfigKey() {
        return self::CONFIG_KEY;
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function initialize(ExtensionManager $extensionManager) {
        // Not used
    }

    /**
     * {@inheritdoc}
     */
    public function configure(ArrayNodeDefinition $builder) {
        $builder
            ->children()
                ->arrayNode('rest')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('apiPrefix')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->defaultValue('api')
                            ->end()
                        ->scalarNode('modelPrefix')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->defaultValue('App\\Models')
                            ->end()
                        ->scalarNode('userModel')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->defaultValue('App\\User')
                            ->end()
                        ->end()
                    ->end()
            ->end();
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function load(ContainerBuilder $container, array $config) {
        // RESTful initializer definition
        $restfulInitializerDefinition = new Definition(
            'Flaxandteal\Bedappy\Context\Initializer\RestfulInitializer',
            [
                $config['rest']['apiPrefix'],
                $config['rest']['modelPrefix'],
                $config['rest']['userModel']
            ]
        );
        $restfulInitializerDefinition->addTag(ContextExtension::INITIALIZER_TAG);

        // Add all definitions to the container
        $container->setDefinition(self::RESTFUL_INITIALIZER_SERVICE_ID, $restfulInitializerDefinition);
    }

    /**
     * {@inheritdoc}
     * @codeCoverageIgnore
     */
    public function process(ContainerBuilder $container) {

    }
}
